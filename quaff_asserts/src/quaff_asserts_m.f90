module quaff_asserts_m
    use Acceleration_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Amount_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Angle_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Area_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Burnup_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Density_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Dynamic_viscosity_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Energy_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Energy_per_amount_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Energy_per_temperature_amount_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Enthalpy_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Force_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Length_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Mass_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Molar_mass_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Power_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Pressure_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Quantity_module_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Speed_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Temperature_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Thermal_conductivity_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Time_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
    use Volume_asserts_m, only: &
            assertEquals, &
            assertEqualsWithinAbsolute, &
            assertEqualsWithinRelative
end module quaff_asserts_m
